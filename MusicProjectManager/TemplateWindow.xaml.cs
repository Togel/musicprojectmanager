﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;

namespace MusicProjectManager
{
    public partial class TemplateWindow : Window
    {
        public TemplateWindow()
        {
            InitializeComponent();
            InitializeTemplateList();
        }

        private void InitializeTemplateList()
        {
            if (System.IO.Directory.Exists(Properties.Settings.Default.Repository + "\\Templates"))
            {
                List<string> Templates = new List<string>();

                foreach (string File in Directory.GetFiles(Properties.Settings.Default.Repository + "\\Templates"))
                {
                    Templates.Add(Path.GetFileNameWithoutExtension(File.Remove(0, File.LastIndexOf('\\') + 1)));
                }

                TemplateList.ItemsSource = Templates;
            }
        }

        private void OnAdd(object Sender, EventArgs Arguments)
        {
            if (TemplateList.SelectedItem != null)
            {
                SelectedTemplate = TemplateList.SelectedItem.ToString();
            }

            Close();
        }

        private void OnCancel(object Sender, EventArgs Arguments)
        {
            Close();
        }

        public string SelectedTemplate { get; set; }
    }
}

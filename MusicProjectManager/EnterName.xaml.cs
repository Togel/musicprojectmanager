﻿using System;
using System.Windows;

namespace MusicProjectManager
{
    public partial class EnterName : Window
    {
        public EnterName()
        {
            InitializeComponent();

            Left = Application.Current.MainWindow.Left + Application.Current.MainWindow.Width / 2 - Width / 2;
            Top = Application.Current.MainWindow.Top + Application.Current.MainWindow.Height / 2 - Height / 2;
        }

        public EnterName(string Default) : this()
        {
            InputBox.Text = Default;
        }

        private void OnApply(object Sender, EventArgs Arguments)
        {
            Input = InputBox.Text;
            Close();
        }

        private void OnCancel(object Sender, EventArgs Arguments)
        {
            Input = null;
            Close();
        }

        public string Input { get; set; }
    }
}

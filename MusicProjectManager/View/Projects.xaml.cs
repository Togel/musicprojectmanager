﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace MusicProjectManager.View
{
    public partial class Projects : UserControl
    {
        public Projects()
        {
            InitializeComponent();
            InitializeProjectList();

            ProjectList.SelectionChanged += OnProjectSelection;
            ProjectList.MouseDoubleClick += OnDoubleClickProject;
            FileList.MouseDoubleClick += OnDoubleClickFile;
        }

        private void InitializeProjectList()
        {
            if (Directory.Exists(Properties.Settings.Default.Repository))
            {
                List<string> Projects = new List<string>();

                foreach (string Directory in Directory.GetDirectories(Properties.Settings.Default.Repository))
                {
                    Projects.Add(Directory.Remove(0, Directory.LastIndexOf('\\') + 1));
                }

                Projects.Remove("Templates");

                ProjectList.ItemsSource = Projects;
            }
        }

        private void OnProjectSelection(object Sender, EventArgs Arguments)
        {
            List<string> Files = new List<string>();

            if (ProjectList.SelectedItem != null)
            {
                string Project = ProjectList.SelectedItem.ToString();

                foreach (string File in Directory.GetFiles(Properties.Settings.Default.Repository + "\\" + Project, "*.ptx"))
                {
                    Files.Add(Path.GetFileNameWithoutExtension(File.Remove(0, File.LastIndexOf('\\') + 1)));
                }

            }

            FileList.ItemsSource = Files;
        }

        private void OnDoubleClickProject(object Sender, EventArgs Arguments)
        {
            if (ProjectList.SelectedItem != null)
            {
                ProjectDetails Details = new ProjectDetails(ProjectList.SelectedItem.ToString());
                Details.Show();
            }
        }

        private void OnDoubleClickFile(object Sender, EventArgs Arguments)
        {
            if (ProjectList.SelectedItem != null && FileList.SelectedItem != null)
            {
                string Project = ProjectList.SelectedItem.ToString();
                string File = FileList.SelectedItem.ToString();
                string Path = Properties.Settings.Default.Repository + "\\" + Project + "\\" + File + ".ptx";

                Process ProTools = new Process();
                ProTools.StartInfo.FileName = Path;

                try
                {
                    ProTools.Start();
                }
                catch { }
            }
        }

        private void OnCreateProject(object Sender, EventArgs Arguments)
        {
            EnterName Dialog = new EnterName();
            Dialog.ShowDialog();

            if (Dialog.Input != null)
            {
                string Path = Properties.Settings.Default.Repository + "\\" + Dialog.Input;

                if (!Directory.Exists(Path))
                {
                    Directory.CreateDirectory(Path);
                    InitializeProjectList();
                }
                else
                {
                    MessageBox.Show("Directory already exists.");
                }
            }
        }

        private void OnDeleteProject(object Sender, EventArgs Arguments)
        {
            if (ProjectList.SelectedItem != null)
            {
                string Project = ProjectList.SelectedItem.ToString();
                string Path = Properties.Settings.Default.Repository + "\\" + Project;

                if (MessageBoxResult.Yes == MessageBox.Show("Do you really want to remove Project " + Project + "?", "Remove Project", MessageBoxButton.YesNo))
                {
                    Directory.Delete(Path, true);
                    InitializeProjectList();
                    OnProjectSelection(Sender, Arguments);
                }
            }
            else
            {
                MessageBox.Show("No Project selected.");
            }
        }

        private void OnRenameProject(object Sender, EventArgs Arguments)
        {
            string ProjectName = ProjectList.SelectedItem.ToString();

            EnterName Dialog = new EnterName(ProjectName);
            Dialog.ShowDialog();

            if (Dialog.Input != null)
            {
                string OldPath = Properties.Settings.Default.Repository + "\\" + ProjectName;
                string NewPath = Properties.Settings.Default.Repository + "\\" + Dialog.Input;

                if (Directory.Exists(NewPath))
                {
                    MessageBox.Show("Project with that name already exists.", "Error");
                }
                else
                {
                    Directory.Move(OldPath, NewPath);
                    InitializeProjectList();
                }
            }
        }

        private void OnRenameFile(object Sender, EventArgs Arguments)
        {
            string FileName = FileList.SelectedItem.ToString();
            string ProjectName = ProjectList.SelectedItem.ToString();

            EnterName Dialog = new EnterName(FileName);
            Dialog.ShowDialog();

            if (Dialog.Input != null)
            {
                string OldPath = Properties.Settings.Default.Repository + "\\" + ProjectName + "\\" + FileName + ".ptx";
                string NewPath = Properties.Settings.Default.Repository + "\\" + ProjectName + "\\" + Dialog.Input + ".ptx";

                if (File.Exists(NewPath))
                {
                    MessageBox.Show("File with that name already exists.", "Error");
                }
                else
                {
                    File.Move(OldPath, NewPath);
                    OnProjectSelection(null, null);
                }
            }
        }

        private void OnAddTemplate(object Sender, EventArgs Arguments)
        {
            if (ProjectList.SelectedItem != null)
            {
                TemplateWindow Dialog = new TemplateWindow();
                Dialog.ShowDialog();

                if (Dialog.SelectedTemplate.Length > 0)
                {
                    string TargetPath = Properties.Settings.Default.Repository + "\\" + ProjectList.SelectedItem.ToString() + "\\" + Dialog.SelectedTemplate + ".ptx";

                    if (!Directory.Exists(TargetPath))
                    {
                        string SourcePath = Properties.Settings.Default.Repository + "\\Templates\\" + Dialog.SelectedTemplate + ".ptx";
                        File.Copy(SourcePath, TargetPath);
                        OnProjectSelection(null, null);
                    }
                    else
                    {
                        MessageBox.Show("Template already exists in Project.", "Error");
                    }
                }
            }
            else
            {
                MessageBox.Show("No Project selected.", "Error");
            }
        }

        private void OnDeleteTemplate(object Sender, EventArgs Arguments)
        {
            if (ProjectList.SelectedItem != null && FileList.SelectedItem != null)
            {
                string Project = ProjectList.SelectedItem.ToString();
                string Filename = FileList.SelectedItem.ToString();
                string Path = Properties.Settings.Default.Repository + "\\" + Project + "\\" + Filename + ".ptx";

                if (MessageBoxResult.Yes == MessageBox.Show("Do you really want to remove " + Filename + " from the project?", "Remove File", MessageBoxButton.YesNo))
                {
                    File.Delete(Path);
                    OnProjectSelection(null, null);
                }
            }
        }
    }
}

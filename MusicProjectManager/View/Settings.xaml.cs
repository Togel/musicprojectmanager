﻿using System.Windows;
using System.Windows.Controls;
using WinForms = System.Windows.Forms;

namespace MusicProjectManager.View
{
    public partial class Settings : UserControl
    {
        public Settings()
        {
            InitializeComponent();
        }

        private void OnPickDirectory(object Sender, RoutedEventArgs Arguments)
        {
            WinForms.FolderBrowserDialog FolderDialog = new WinForms.FolderBrowserDialog();
            FolderDialog.SelectedPath = Properties.Settings.Default.Repository;

            if (WinForms.DialogResult.OK == FolderDialog.ShowDialog())
            {
                Properties.Settings.Default.Repository = FolderDialog.SelectedPath;
            }
        }
    }
}

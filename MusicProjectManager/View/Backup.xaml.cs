﻿using System;
using System.IO.Compression;
using System.Windows.Controls;
using WinForms = System.Windows.Forms;

namespace MusicProjectManager.View
{
    public partial class Backup : UserControl
    {
        public Backup()
        {
            InitializeComponent();
        }

        private void OnStore(object Sender, EventArgs Arguments)
        {
            WinForms.FolderBrowserDialog FolderDialog = new WinForms.FolderBrowserDialog();

            if (WinForms.DialogResult.OK == FolderDialog.ShowDialog())
            {
                ZipFile.CreateFromDirectory(Properties.Settings.Default.Repository, FolderDialog.SelectedPath + "\\Backup.zip");
            }
        }

        private void OnRestore(object Sender, EventArgs Arguments)
        {
            WinForms.OpenFileDialog Dialog = new WinForms.OpenFileDialog();

            if (WinForms.DialogResult.OK == Dialog.ShowDialog())
            {
                ZipFile.ExtractToDirectory(Dialog.FileName, Properties.Settings.Default.Repository);
            }
        }
    }
}

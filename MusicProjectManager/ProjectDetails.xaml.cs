﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;

namespace MusicProjectManager
{
    public partial class ProjectDetails : Window
    {
        public ProjectDetails(string ProjectName)
        {
            Project = ProjectName;

            InitializeComponent();
            InitializeCommentList();
            InitializeAudioList();

            AudioList.MouseDoubleClick += OnDoubleClickAudioList;

            DispatcherTimer Timer = new DispatcherTimer();
            Timer.Interval = TimeSpan.FromSeconds(1);
            Timer.Tick += OnTimer;
            Timer.Start();

        }

        private void InitializeAudioList()
        {
            string AudioFilesPath = Properties.Settings.Default.Repository + "\\" + Project + "\\Audio Files";

            if (Directory.Exists(AudioFilesPath))
            {
                List<string> Files = new List<string>();

                foreach (string File in Directory.GetFiles(AudioFilesPath, "*.wav"))
                {
                    Files.Add(Path.GetFileNameWithoutExtension(File.Remove(0, File.LastIndexOf('\\') + 1)));
                }

                AudioList.ItemsSource = Files;
            }
        }

        private void InitializeCommentList()
        {
            if (Directory.Exists(Properties.Settings.Default.Repository))
            {
                string CommentsPath = Properties.Settings.Default.Repository + "\\" + Project + "\\Comments.txt";

                if (File.Exists(CommentsPath))
                {
                    List<string> Comments = new List<string>();
                    StreamReader Reader = new StreamReader(CommentsPath);
                    string Comment = string.Empty;

                    while ((Comment = Reader.ReadLine()) != null)
                    {
                        Comments.Add(Comment);
                    }

                    CommentList.ItemsSource = Comments;
                    Reader.Close();
                }
                else
                {
                    CommentList.ItemsSource = null;
                }
            }
        }

        private void OnTimer(object sender, EventArgs Arguments)
        {
            if (AudioPlayer.Source != null && AudioPlayer.NaturalDuration.HasTimeSpan)
            {
                AudioProgress.Maximum = AudioPlayer.NaturalDuration.TimeSpan.TotalSeconds;
                AudioProgress.Value = AudioPlayer.Position.TotalSeconds;
                AudioProgressText.Text = string.Format("{0} / {1}", AudioPlayer.Position.ToString(@"mm\:ss"), AudioPlayer.NaturalDuration.TimeSpan.ToString(@"mm\:ss"));
            }
            else
            {
                AudioProgress.Maximum = 1.0;
                AudioProgress.Value = 0.0;
                AudioProgressText.Text = string.Empty;
            }
        }

        private void OnDoubleClickAudioList(object Sender, EventArgs Arguments)
        {
            if (Directory.Exists(Properties.Settings.Default.Repository))
            {
                string Path = Properties.Settings.Default.Repository + "\\" + Project + "\\Audio Files\\" + AudioList.SelectedItem.ToString() + ".wav";

                if (File.Exists(Path))
                {
                    AudioPlayer.Open(new Uri(Path));
                    OnPlay(Sender, Arguments);
                }
                else
                {
                    MessageBox.Show("Audio File does not exists anymore.");
                }
            }
            else
            {
                MessageBox.Show("Repository is invalid. Check Settings!");
            }
        }

        private void OnPlay(object Sender, EventArgs Arguments)
        {
            AudioPlayer.Play();
        }

        private void OnPause(object Sender, EventArgs Arguments)
        {
            AudioPlayer.Pause();
        }

        private void OnStop(object Sender, EventArgs Arguments)
        {
            AudioPlayer.Stop();
        }

        private void OnAdd(object Sender, EventArgs Arguments)
        {
            if (Directory.Exists(Properties.Settings.Default.Repository))
            {
                EnterName Dialog = new EnterName();
                Dialog.ShowDialog();

                if (Dialog.Input != null)
                {
                    string CommentsPath = Properties.Settings.Default.Repository + "\\" + Project + "\\Comments.txt";

                    if (!File.Exists(CommentsPath))
                    {
                        File.Create(CommentsPath).Close();
                    }

                    File.AppendAllText(CommentsPath, Dialog.Input + Environment.NewLine);
                    InitializeCommentList();
                }
            }
        }

        private void OnRemove(object Sender, EventArgs Arguments)
        {
            if (CommentList.SelectedItem != null)
            {
                if (MessageBoxResult.Yes == MessageBox.Show("Do you really want to remove the Comment?\n" + CommentList.SelectedItem.ToString(), "Remove Comment", MessageBoxButton.YesNo))
                {
                    string CommentsPath = Properties.Settings.Default.Repository + "\\" + Project + "\\Comments.txt";
                    string TempPath = Path.GetTempFileName();

                    using (StreamReader Reader = new StreamReader(CommentsPath))
                    using (StreamWriter Writer = new StreamWriter(TempPath))
                    {
                        string Line = string.Empty;

                        while ((Line = Reader.ReadLine()) != null)
                        {
                            if (Line != CommentList.SelectedItem.ToString())
                            {
                                Writer.WriteLine(Line);
                            }
                        }

                        Reader.Close();
                        Writer.Close();
                        File.Delete(CommentsPath);
                        File.Move(TempPath, CommentsPath);

                        if (new FileInfo(CommentsPath).Length == 0)
                        {
                            File.Delete(CommentsPath);
                        }

                        InitializeCommentList();
                    }
                }
            }
            else
            {
                MessageBox.Show("No Comment selected.");
            }
        }

        private string Project = string.Empty;
        private MediaPlayer AudioPlayer = new MediaPlayer();
    }
}

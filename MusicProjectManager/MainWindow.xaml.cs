﻿using MusicProjectManager.ViewModels;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace MusicProjectManager
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new ProjectsModel();
            SetActiveButton(ProjectsButton);
        }

        private void OnExit(object Sender, System.ComponentModel.CancelEventArgs Arguments)
        {
            Properties.Settings.Default.Save();
        }

        private void ClickedProjects(object Sender, RoutedEventArgs Arguments)
        {
            DataContext = new ProjectsModel();
            SetActiveButton(ProjectsButton);
        }

        private void ClickedTemplates(object Sender, RoutedEventArgs Arguments)
        {
            DataContext = new TemplatesModel();
            SetActiveButton(TemplatesButton);
        }

        private void ClickedBackup(object Sender, RoutedEventArgs Arguments)
        {
            DataContext = new BackupModel();
            SetActiveButton(BackupButton);
        }

        private void ClickedSettings(object Sender, RoutedEventArgs EventArguments)
        {
            DataContext = new SettingsModel();
            SetActiveButton(SettingsButton);
        }

        private void SetActiveButton(Button MenuButton)
        {
            ResetButtonColors();
            MenuButton.Background = new SolidColorBrush(Color.FromArgb(0xFF, 0x22, 0x22, 0x22));
        }

        private void ResetButtonColors()
        {
            ProjectsButton.Background = new SolidColorBrush(Color.FromArgb(0xFF, 0x33, 0x33, 0x33));
            TemplatesButton.Background = new SolidColorBrush(Color.FromArgb(0xFF, 0x33, 0x33, 0x33));
            BackupButton.Background = new SolidColorBrush(Color.FromArgb(0xFF, 0x33, 0x33, 0x33));
            SettingsButton.Background = new SolidColorBrush(Color.FromArgb(0xFF, 0x33, 0x33, 0x33));
        }
    }
}
